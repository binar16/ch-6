const { user_bio, user_game } = require("../models");
const users = require("../data/users.json");

exports.relogin = (req, res) => {
  res.redirect("/login");
};

exports.login = async (req, res) => {
  res.render("login");
};

exports.posLogin = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const superAuth = await users.find(
    (data) => data.username === username && data.password == password
  );
  const check = await user_game.findOne({
    where: { username, password },
  });
  if (check || superAuth) {
    res.redirect("/get");
  } else {
    res.redirect("/login");
  }
};

exports.bio = async (req, res) => {
  const artikel = await user_bio.findAll({
    include: user_game,
  });
  res.render("end/biodata.ejs", { artikel: artikel });
};

exports.dashboard = async (req, res) => {
  const isi = await user_game.findAll();

  res.render("end/dashboard.ejs", { isi: isi });
};

exports.create = async (req, res) => {
  const isi = await user_game.findAll();
  res.render("form/create", { isi: isi });
};

exports.posCreate = async (req, res) => {
  const makeArtikel = await user_bio.create(req.body);
  res.redirect("/get");
};
exports.profil = async (req, res) => {
  res.render("form/profil");
};

exports.posProfil = async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  if (username && password) {
    const makeProfil = await user_game.create(req.body);
    res.redirect("/dashboard");
  } else {
    res.redirect("/dashboard");
  }
};

exports.detail = async (req, res) => {
  const user = await user_bio.findOne({
    where: {
      id: req.params.id,
    },
  });

  res.render("form/detail", { user });
};

exports.edit = async (req, res) => {
  const id = req.params.id;
  const user = await user_bio.findOne({
    where: { id },
  });
  res.render("form/edit", { user });
};

exports.posEdit = async (req, res) => {
  const id = req.body.id;
  const user = await user_bio.findOne({
    where: { id },
  });
  await user.update(req.body);
  await user.save();
  res.redirect("/get");
};

exports.delete = async (req, res) => {
  const destroy = await user_bio.destroy({
    where: {
      id: req.params.id,
    },
  });
  const destroyed = await user_game.destroy({
    where: {
      id: req.params.id,
    },
  });
  if (destroy) {
    res.redirect("/get");
  }
  if (destroyed) {
    res.redirect("/dashboard");
  }
};
