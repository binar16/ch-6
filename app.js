const express = require("express");
const bodyParser = require("body-parser");
const moduleList = require("./modules/module")

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(express.static("assets"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.get("/", moduleList.relogin)

app.get("/login", moduleList.login);

app.post("/login", moduleList.posLogin);

app.get("/get", moduleList.bio);

app.get("/dashboard", moduleList.dashboard);

app.get("/create", moduleList.create);

app.post("/create", moduleList.posCreate);

app.get("/profil", moduleList.profil);

app.post("/profil", moduleList.posProfil);

app.get("/get/:id", moduleList.detail);

app.get("/edit/:id", moduleList.edit);

app.post("/edit", moduleList.posEdit);

app.get("/delete/:id", moduleList.delete);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
