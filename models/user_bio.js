'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_bio extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
     static associate(models) {
      models.user_bio.belongsTo(models.user_game, {
        foreignKey: "userId",
        onDelete: 'cascade',
        onUpdate: 'cascade',
        hooks: true
      }); 
    }
  }
  user_bio.init({
    hobi: DataTypes.STRING,
    jalan: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user_bio',
  });
  return user_bio;
};